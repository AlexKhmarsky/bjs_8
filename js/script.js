// Теоретичні питання
//
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// Об'єктна модель документа, це стандарт який дозволяє працювати з розміткою документів,
// з html, xml змінюючи їх структуру.
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//    innerHTML виводиться з усім контентом та тегами, innerText виводить увесь контент але ігнорує теги.
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//    querySelector та querySelectorAll кращий і зручний спосіб. Ще можна звернутись
//    getElementById - пошук по id, getElementsBy(TagName/ClassName/Name) - пошук по тегу, класу, атрибуту.
//     Завдання
//    Код для завдань лежить в папці project.
//
// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//
// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//
// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
//
// 4. Отримати елементи
//
// 5. , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// 6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let paragraph = document.querySelectorAll('p');

paragraph.forEach(p => {
    p.style.backgroundColor = '#ff0000';
});
let optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.childNodes);
if (optionsList.childNodes) {
    optionsList.childNodes.forEach(test => {
        let testType;
        if(test.testType === 1) {
            testType = 'All node elements';
        } else if (test.testType === 3) {
            testType = 'All node texts';
        } else if (test.testType === 8) {
            testType = 'All node comments';
        }
        console.log(`${test.nodeName} - ${testType}`);
    })
};
let testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is paragraph';

let headerElements = document.querySelector('.main-header').children;
for (let element of headerElements) {
    console.log(element);
    element.classList.add('nav-item');
};
let sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(element => element.classList.remove('section-title'));